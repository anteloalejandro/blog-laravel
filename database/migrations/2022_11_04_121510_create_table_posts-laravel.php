<?php

use App\Models\Author;
use App\Models\Post;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('posts_laravel', function (Blueprint $table) {
      $table->id('post_id');
      $table->string('post_title', 100);
      $table->string('post_slug', 100);
      $table->string('post_abstract', 255);
      $table->text('post_body');
      $table->enum('post_visible', ['Mostrar', 'No mostrar']);
      $table->string('post_image', 200);
      $table->integer('author_id');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('posts-laravel');
  }
};
