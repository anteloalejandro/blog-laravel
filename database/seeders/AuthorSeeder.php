<?php

namespace Database\Seeders;

use Database\Factories\AuthorSeederFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $author = new \App\Models\Author();
        $author->author_name = 'Anónimo';
        $author->save();

        \App\Models\Author::factory()->count(5)->create();
    }
}
