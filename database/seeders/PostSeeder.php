<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $post = new \App\Models\Post();
        $post->author()->associate(\App\Models\Author::findOrFail(1));
        $post->post_title = "Las mejores Ferias textiles del invierno 2016";
        $post->post_slug = "las-mejores-ferias-textiles-del-invierno-2016";
        $post->post_abstract = "Las mejores ferias del sector textil y del hogar de este invierno.";
        $post->post_visible = "Mostrar";
        $post->post_image = "a004b-ferias-textiles.jpg";
        $post->post_body = '
            <p>Si hay algo que nos encanta en <strong>Aquaclean,</strong> son las <strong>ferias textiles internacionales</strong>, donde se reúnen propuestas, compradores, <em>startups</em>, grandes empresas y compañeros de todos los rincones del mundo.</p>
            <p><img style="display: block; margin-left: auto; margin-right: auto;" src="http://aquaclean.com/assets/grocery_crud/texteditor/tinymce_4/responsive_filemanager/uploads/Ferias%20textiles.jpg" alt="Ferias textiles" /></p>
            <p>Este tipo de eventos son un <strong>gran escaparate de las últimas tecnologías y avances</strong>, así como <strong>de las tendencias</strong> que están por venir, con lo que dejarse caer por alguna de ellas, es sin duda una forma de llenarnos de inspiración e ideas, y porque no, de hacer un poco de <strong>networking</strong> para algún posible proyecto futuro.</p>
            <p>Por todo ello, hoy te traemos algunas de las <strong>ferias textiles, de decoración e interiorismo más interesantes</strong> que se celebran este <strong>próximo invierno</strong> alrededor del mundo. Empieza a reservar vuelos, ¡que nos vamos!</p>
            <p><img src="http://aquaclean.com/assets/grocery_crud/texteditor/tinymce_4/responsive_filemanager/uploads/Ferias%20textiles_1.jpg" alt="Ferias textiles_1" /></p>
            <h2><strong>Heimtextil 2016</strong></h2>
            <p>Nuestro primer destino es la ciudad alemana de <strong>Frankfurt</strong>, donde entre los días <strong>12 y 15 de enero</strong> se celebra <strong>Heimtextil 2016</strong>. Con más de 40 años de trayectoria, <strong>Heimtextil </strong>es una de las más prestigiosas y conocidas  ferias a nivel mundial de <strong>textiles para el hogar y el sector contract</strong>. Solo el año pasado contó con más de 2.700 expositores, entre los que se encontraba <strong>Aquaclean</strong>, y cerca de 67.000 visitantes.</p>
            <p><strong>Heimtextil </strong>es la primera gran fecha que da inicio a las ferias textiles. Se ha convertido sin duda en la plataforma más grande y más importante para los fabricantes, minoristas y diseñadores de todo el mundo, por lo que si quieres estar a la última en tendencias, no te quedará más remedio que darte una vuelta por allí.</p>
            <p>Un año más, <strong>Aquaclean </strong>repite en <strong>Heimtextil 2016</strong>, así que si os pasáis por allí, no dejéis de visitarnos <em>(Hall 4.1 B61A).</em></p>
            <h2><strong>Imm Cologne</strong></h2>
            <p>Y corriendo, volvemos a Alemania, para encontramos entre los días <strong>18 y 24 de enero</strong> la <strong>Imm Cologne,</strong> la primera feria de interiorismo del año. En ella se presentan las tendencias que marcarán las pautas en el sector del diseño y la decoración de interiores a lo largo de todo el 2016.</p>
            <p><img style="display: block; margin-left: auto; margin-right: auto;" src="http://aquaclean.com/assets/grocery_crud/texteditor/tinymce_4/responsive_filemanager/uploads/Ferias%20textiles_2.jpg" alt="Ferias textiles_2" /></p>
            <p>En <strong>Imm Cologne</strong> podrás encontrar una variedad única de ideas para espacios diversos, según necesidades y estilos. Desde conceptos básicos más cercanos al consumidor final medio, hasta artículos y muebles de diseño <em>high level</em>. Una cita ineludible para todos aquellos que queremos estar al tanto de las tendencias, en el ámbito de la decoración, que nos sorprenderán el año que viene.</p>
            <p><img src="http://aquaclean.com/assets/grocery_crud/texteditor/tinymce_4/responsive_filemanager/uploads/Ferias%20textiles_3.jpg" alt="Ferias textiles_3" /></p>
            <p>Se trata de una <strong>plataforma perfecta</strong> para conocer todo lo referente a la <strong>tecnología aplicada</strong> a los artículos textiles: impresiones, transferencias de bordados, flocado, aplicaciones de diamantes de imitación o grabado láser, entre otros. Desde expertos hasta compradores.</p>
            <p>Si no quieres perder la oportunidad, reserva vuelos, porque con más de 260 expositores, se espera que se congreguen hasta 12.000 visitantes ávidos de conocer los últimos avances en tecnología textil.</p>
            <p>El invierno se presenta calentito. Así que recuerda nombres y fechas y resérvalo en la agenda, porque perderse estas <strong>ferias internacionales</strong> no es una opción. ¡Nos vemos allí!</p>
            <p> </p>
            ';
        $post->save();

        \App\Models\Post::factory()->count(5)->create();
    }
}
