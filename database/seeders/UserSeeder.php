<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $usuario = new User();
      $usuario->name = 'admin';
      $usuario->email = 'admin@localhost';
      $usuario->password = bcrypt('admin');
      $usuario->save();
    }
}
