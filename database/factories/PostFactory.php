<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
  {
    $num_authors = \App\Models\Author::count();
    return [
      'post_title' => $this->faker->text(50),
      'post_slug' => "",
      'post_abstract' => "",
      'post_visible' => "Mostrar",
      'post_image' => "",
      'post_body' => $this->faker->text(1000),
      'author_id' => rand(1, $num_authors)
    ];
  }
}
