@extends('plantilla')
@section('titulo', 'Inicio')

@section('contenido')
@if (session()->has('msg'))
<div class="alert alert-{{session('msg-type')}} alert-dismissible fade show" role="alert">
  {{session('msg')}}
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif
@if (auth()->check())
  <p><small>¡Bienvenido, {{auth()->user()->name}}!</small></p>
@endif
<table class="table">
  @if (!$posts->isEmpty())
    <thead>
      <tr>
        <th>Post</th>
        <th>Imagen</th>
        @if (auth()->check())
          <th>Acciones</th>
        @endif
      </tr>
    </thead>
    <tbody>
      @foreach ( $posts as $post )
        <tr>
          <td class="align-middle">
            <a href="{{ route('show', $post->post_id) }}"> {{$post->post_title}}</a>
          </td>
          <td class="align-middle">
            <img src="/storage/img/{{$post->post_image}}" alt="{{empty($post->post_image)?'No hay imagen':$post->post_image}}">
          </td>
          @if (auth()->check())
            <td class="align-middle">
              <div>
                <form action="{{ route('destroy', $post->post_id) }}" method="POST">
                  @method('DELETE')
                  @csrf
                  <button type="submit" class="btn btn-danger" onclick="return confirm('¿Seguro que quieres borrar el post?')">Borrar</button>
                </form>
              </div>
              <div>
                <a href="{{ route('edit', $post->post_id) }}">
                  <button class="btn btn-danger">Modificar</button>
                </a>
              </div>
            </td>
          @endif
        </tr>
      @endforeach
    </tbody>
  @else
    <tr>
      <td id="error">No hay posts</td>
    </tr>
  @endif
</table>
{{$posts->links()}}
@endsection

@section('estilo')
  <style>
    table.table {
      border-color: black;
    }

    table td:not(#error) {
      height: 200px;
    }

    table td:nth-of-type(2) {
      text-align: center
    }
    table td:nth-of-type(3) button {
      width: 100%;
    }
    table td:nth-of-type(3) button:first-of-type {
      margin-bottom: 0.25em;
    }

    a {
      color: black;
      font-weight: bold;
      text-decoration: none;
    }
    a:visited {
      color: grey;
    }
  </style>
@endsection
