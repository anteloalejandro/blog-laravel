<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  @vite(['resources/sass/app.scss', 'resources/js/app.js'])
  @yield('estilo')
  <title>
    @yield('titulo')
  </title>
</head>
<body class="bg-light">
  <header class="bg-dark">
    @include('partials.nav')
  </header>
  <div id="container" class="bg-light">
    @yield('contenido')
  </div>
</body>
</html>
