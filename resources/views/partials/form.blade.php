@extends('plantilla')
@section('contenido')
  @if ($errors->any())
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
      <h5>¡Ha habido problemas!</h5>
    @foreach ($errors->all() as $error)
      <hr class="mb-0 mt-0">
      <p class="mb-0 mt-0">{{$error}}</p>
    @endforeach
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  @endif
  <form action="@yield('action')" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group mb-3">
      <label for="titulo">Titulo: </label>
      @if(old('titulo') !== null)
        <?php $titulo = old('titulo') ?>
      @else
        @isset($post)
          <?php $titulo = $post->post_title?>
        @else
          <?php $titulo = null ?>
        @endisset
      @endif
      <input type="text" id="titulo" name="titulo" value="{{$titulo}}" required class="form-control">
    </div>
    <div class="form-group mb-3">
      <label for="cuerpo">Cuerpo</label>
      @if(old('cuerpo') !== null)
      <?php $cuerpo = old('cuerpo') ?>
      @else
        @isset($post)
          <?php $cuerpo = $post->post_body?>
        @else
          <?php $cuerpo = null ?>
        @endisset
      @endif
      <textarea name="cuerpo" id="cuerpo" class="form-control" required rows="25">{{$cuerpo}}</textarea>
    </div>
    <div class="form-group mb-3">
      <label for="imagen">Imagen: </label>
      <input type="file" name="imagen" id="imagen" class="form-control">
      <small>@yield('img-msg')</small>
    </div>
    <div class="form-gorup mb-3">
      <label for="autor">Autor: </label>
      <select name="autores" id="autores" class="form-control">
      @isset($post)
        <?php $authors = App\Models\Author::orderBy('author_name', 'ASC')->get() ?>
      @endisset
      @isset($authors)
        @foreach ($authors as $author)
          <?php
          if (old('autores') !== null)
            $id = old('autores');
          else
            $id = isset($post) ? $post->author_id : 1;
          ?>
          <option value="{{$author->id}}" {{$author->id == $id ? 'selected': null}}>
            {{$author->author_name}}
          </option>
        @endforeach
      @endisset
      </select>
    </div>
    <div class="form-group">
      <input class="btn @yield('button-class') w-100" type="submit" name="@yield('button')" value="@yield('button')">
    </div>
  </form>
@endsection

@section('estilo')
  <style>
    form {
      margin: auto;
      max-width: 600px;
      display: flex;
      flex-flow: column;
      align-items: center;
    }
    form>div {
      width: 100%;
    }

    small {
      color: darkslategray;
    }
  </style>
@endsection
