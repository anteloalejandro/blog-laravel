<nav class="navbar navbar-expand-sm navbar-dark">
  <div class="container-fluid">
    <span class="navbar-brand">Blog de noticias</span>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="{{ route('index') }}">Inicio</a>
        </li>
        @if(auth()->check())
          <li class="nav-item">
            <a href="{{route('create')}}" class="nav-link active text-primary border border-primary rounded-2">
              Insertar post
            </a>
          </li>
          <li class="nav-item position-absolute end-0">
            <a href="{{route('logout')}}" class="nav-link active align-right">
              Cerrar sesión
            </a>
          </li>
        @else
          <li class="nav-item position-absolute end-0">
            <a href="{{route('login')}}" class="nav-link active align-right">
              Iniciar Sesión
            </a>
          </li>
        @endif
      </ul>
    </div>
  </div>
</nav>
