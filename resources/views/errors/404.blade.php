@extends('plantilla')
@section('titulo', 'Error 404')

@section('contenido')
  <div class="center">
    <h1>ERROR 404</h1>
    <p>Documento no encontrado</p>
  </div>
@endsection

@section('estilo')
  <style>
    div.center {
      position: absolute;
      top: 40%;
      left: 0;
      width: 100vw;
      padding: 3rem;
      text-align: center;
    }
  </style>
@endsection
