@extends('plantilla')

@section('titulo')

@section('contenido')
  <h1>Inicio de sesión</h1>
  @isset($error)
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
      {{$error}}
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  @endisset
  <form action="{{route('login')}}" method="POST">
    {{ csrf_field() }}
    {{-- <div class="form-group mb-3">
      <label for="usuario">Nombre de usuario</label>
      <input type="text" name="usuario" id="usuario" class="form-control">
    </div> --}}
    <div class="form-group mb-3">
      <label for="correo">Correo electrónico</label>
      <input type="email" name="correo" id="correo" class="form-control">
    </div>
    <div class="form-group mb-3">
      <label for="clave">Contraseña</label>
      <input type="password" name="clave" id="clave" class="form-control">
    </div>
    <div class="form-group">
      <input type="submit" value="Iniciar sesión" class="btn btn-primary form-control">
    </div>
  </form>
@endsection

@section('estilo')
  <style>
    form {
      margin: auto;
      max-width: 600px;
      display: flex;
      flex-flow: column;
      align-items: center;
    }
    form>div {
      width: 100%;
    }

    small {
      color: darkslategray;
    }
  </style>
@endsection
