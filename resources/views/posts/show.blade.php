@extends('plantilla')

@section('titulo')
{{ $post->post_title }}
@endsection

@section('contenido')
  <h1>  {{ $post->post_title }}</h1>
  <p><small>Autor: {{ $post->author->author_name }}</small></p>
  {{--
    Esto:
    {{ $post->post_body }}
    También muestra las etiquetas HTML.
    Hace falta usar PHP.
  --}}
  <?= $post->post_body ?>
@endsection

@section('estilo')
<style>
    img {
      width: 100%;
    }
</style>
@endsection
