@extends('partials.form')

@section('action')
  {{ route('store') }}
@endsection

@section('titulo', 'Insertar post')

@section('button', 'Insertar')

@section('button-class', 'btn-primary')

@section('img-msg', 'Se recomienda poner una imagen para que el post destaque más')
