@extends('partials.form')

@section('action')
  {{ route('update', $post->post_id) }}
@endsection

@section('titulo', 'Modificar post')

@section('button', 'Modificar')

@section('button-class', 'btn-danger')

@section('img-msg', 'Si no se sube ninguna imagen, se usará la imagen actual')
