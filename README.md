# Blog con noticias hecho con el framework laravel

## Requisitos

- PHP
- Laravel
- Composer
- npm

## Intalación

- `git clone https://gitlab.com/anteloalejandro/blog-laravel`
- `cd blog-laravel`
- `composer install`
- `npm install && npm run build`
- `cp .env.example .env`

## Ejecución

- `npm run dev & php artisan serve`
