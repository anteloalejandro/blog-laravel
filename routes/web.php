<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\StorageController;
use App\Models\Post;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/', PostController::class)->except(['show', 'update', 'destroy', 'edit']);
Route::get('post/{id}',
  [PostController::class, 'show']
)->where('id', '[0-9]+')->name('show');

Route::post('update/{id}',
  [PostController::class, 'update']
)->where('id', '[0-9]+')->name('update')->middleware('auth');

Route::delete('destroy/{id}',
  [PostController::class, 'destroy']
)->where('id', '[0-9]+')->name('destroy')->middleware('auth');

Route::get('modificar/{id}',
  [PostController::class, 'edit']
)->where('id', '[0-9]+')->name('edit')->middleware('auth');

Route::post('/store', [PostController::class, 'store'])->name('store');

Route::get('login', [LoginController::class, 'loginForm'])->name('login');
Route::post('login', [LoginController::class, 'login'])->name('login');
Route::get('logout', [LoginController::class, 'logout'])->name('logout');
