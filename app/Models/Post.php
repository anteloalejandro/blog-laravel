<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $table = 'posts_laravel';
    protected $primaryKey = 'post_id';
    protected $fillable = [
      'post_title',
      'post_slug',
      'post_abstract',
      'post_body',
      'post_date',
      'post_visible',
      'post_image'
    ];

    public function author() {
      return $this->belongsTo('App\Models\Author');
    }
}
