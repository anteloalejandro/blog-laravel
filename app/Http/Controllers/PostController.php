<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  private $validation = [
    'titulo' => 'required|min:3',
    'cuerpo' => 'required',
    'autores' => 'required|numeric'
  ];
  private $validation_msg = [
    'titulo.required' => 'El título es obligatorio',
    'cuerpo.required' => 'El cuerpo es obligatorio',
    'autores.required' => 'El autor es obligatorio',
  ];
  public function index()
  {
    $posts = Post::orderBy('created_at', 'DESC')->paginate(3);
    return view('inicio', compact('posts'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $authors = Author::orderBy('author_name', 'ASC')->get();
    return view('posts.create', compact('authors'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    request()->validate(
      $this->validation,
      $this->validation_msg
    );

    $post = new Post();
    $author = Author::findOrFail($request->input('autores'));
    $post->author()->associate($author);
    $post->post_slug = "";
    $post->post_abstract = "";
    $post->post_visible = "Mostrar";

    $post->post_title = $request->input('titulo');
    $post->post_body = $request->input('cuerpo');

    $img = $request->file('imagen');
    if (isset($img)) {
      $nombre_img = $img->getClientOriginalName();

      if(!Storage::disk('img')->put($nombre_img, File::get($img)))
        dd('Error al subir la imagen');
    } else {
      $nombre_img = '';
    }
    $post->post_image = $nombre_img;

    $post->save();
    return redirect()->route('index')->
    with('msg', 'Post guardado exitosamente')->
    with('msg-type', 'success');
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\Post  $post
   * @return \Illuminate\Http\Response
   */
  // public function show(Post $post)
  public function show(int $id)
  {
    $post = Post::findOrFail($id);
    return view('posts.show', compact('post'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Models\Post  $post
   * @return \Illuminate\Http\Response
   */
  public function edit(int $id)
  {
    $post = Post::findOrFail($id);
    return view('posts.edit', compact('post'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\Post  $post
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, int $id)
  {
    request()->validate(
      $this->validation,
      $this->validation_msg
    );

    $post = Post::findOrFail($id);
    $post->post_title = $request->input('titulo');
    $post->post_body = $request->input('cuerpo');
    $post->author_id = $request->input('autores');
    $img = $request->file('imagen');

    if (isset($img)) {
      $nombre_img = $img->getClientOriginalName();
      $post->post_image = $nombre_img;
      if(!Storage::disk('img')->put($nombre_img, File::get($img)))
        dd('Error al subir la imagen');
    }

    $post->save();
    return redirect()->route('index')->
    with('msg', 'Post modificado exitosamente')->
    with('msg-type', 'success');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\Post  $post
   * @return \Illuminate\Http\Response
   */
  public function destroy(int $id)
  {
    Post::findOrFail($id)->delete();
    return redirect()->route('index')->
    with('msg', 'Post eliminado exitosamente')->
    with('msg-type', 'success');
  }
}
