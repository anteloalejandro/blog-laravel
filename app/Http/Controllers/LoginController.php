<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
  public function logout() {
    Auth::logout();
    return redirect('/');
  }
  public function loginForm() {
    return view('auth.login');
  }
  public function login(Request $request) {
    $user = $request->input('usuario');
    $email = $request->input('correo');
    $passwd = $request->input('clave');

    if (Auth::attempt(['email' => $email, 'password' => $passwd]))
      return redirect('/');
    else
      $error = 'Inicio de sesión incorrecto';
      return view('auth.login', compact('error'));

  }
}
