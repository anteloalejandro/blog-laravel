<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class StorageController extends Controller
{
    public function upload(Request $request) {
      $file = $request->file('archivo');

      $nombre = $file->getClientOriginalName();

      Storage::disk('img')->put($nombre, File::get($file));
      dd('Subido correctamente');
    }
}
